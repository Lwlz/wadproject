import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  //styleUrls: ['./nav.component.scss']
  styleUrls: ['../../assets/css/module/header.css']
})
export class NavComponent implements OnInit {

  appTitle: String = "lwlz";
  
  constructor() { }

  ngOnInit() {
  }

}
