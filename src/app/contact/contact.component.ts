import { Component, OnInit } from '@angular/core';
import { FormBuilder , FormGroup , Validator, Validators }from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  //styleUrls: ['./contact.component.scss']
  // styleUrls: ['../../assets/sass/style.scss']
  styleUrls: ['../../assets/css/module/contact.css']
})
export class ContactComponent implements OnInit {

  messageForm : FormGroup;
  submitted= false;
  sucess = false;

  constructor(private formBuilder:FormBuilder) {

    this.messageForm = this.formBuilder.group({
      
      name: ['',Validators.required],
      message: ['',Validators.required]

    })
   }

/* 
   constructor() {} */
   

   onSubmit(){

    this.submitted = true;

    if(this.messageForm.invalid){
      return;
    }

    this.sucess = true;
   }

  ngOnInit() {
  }

}
