import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {DataService} from './data.service' //added this 

import {HttpClientModule} from '@angular/common/http'
import {ReactiveFormsModule} from '@angular/forms'
//added
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { SalesComponent } from './sales/sales.component';
import { StatsComponent } from './stats/stats.component';
import { ClientsComponent } from './clients/clients.component';
import { NavigationComponent } from './navigation/navigation.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    AboutComponent, 
    ContactComponent,
    HomeComponent,
    FooterComponent,
    DashboardComponent,
    LoginComponent,
    InscriptionComponent,
    SalesComponent,
    StatsComponent,
    ClientsComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule ,//added
    ReactiveFormsModule//added
  ],
  providers: [DataService], //dataservice added here 
  bootstrap: [AppComponent]
})
export class AppModule { }
