import { Component, OnInit } from '@angular/core';
// added
 import {HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['../../assets/css/module/dashboard.css']
})
export class SalesComponent implements OnInit {

  // private sales =[

  //   {

  //     id : "1",
  //     productId:"22332",
  //     productName : "knives",
  //     clientId : "zz",
  //     clientName : "zz",
  //     createdDate : "12-12-12",
  //     quantity : "90",
  //     categoryId : "other",
  //     category : "other",
  //     sectorId : "camp",
  //     tva : "8%",
  //     ht : "8$"
  //   },
  
  // ]

  constructor(private http : HttpClient) { }

  ngOnInit(){
    this.loadSalesForUser('5bffdb5a27839448689ae715');
  }

  private sales = undefined;

  loadSalesForUser(userId){

    if(userId == undefined){ return; }

    this.http.get('http://127.0.0.1:3000/rest/Sales?userId=' + userId)
            .toPromise()
            .then((res: Response) => {
              console.log(res);
              this.sales = res
            });
  }

}
