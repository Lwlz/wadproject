import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['../../assets/css/module/dashboard.css']
})
export class StatsComponent implements OnInit {

  private stats =[
    {
      id : '1',
      product : "twix",
      quantity : "7",
      client : "john doe",
      category :"sweets",
      field : "food"
    },
    {
      id : '2',
      product : "twix",
      quantity : "8",
      client : "zz",
      category :"",
      field : "groceries"
    },
    {
      id : '3',
      product : "twix",
      quantity : "joibjk",
      client : "jhhjjk",
      category :"hghjhjk",
      field : "catebvb"
    },
    {
      id : '4',
      product : "twix",
      quantity : "joibjk",
      client : "jhhjjk",
      category :"hghjhjk",
      field : "catebvb"
    },
  ]


  constructor() { }

  ngOnInit() {
  }

}
