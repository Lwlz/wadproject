import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { chart } from 'chart.js' //added this 
import { WheatherServiceService } from '../wheather-service.service';
// import { WheatherServiceService } from '../wheather-service.service';
import { map } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { of } from 'rxjs';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['../../assets/css/module/dashboard.css']
})
export class DashboardComponent implements OnInit {


  users: Object;

 /*  private whether =[

    {

      id : "1",
      productId:"22332",
      productName : "knives",
      clientId : "zz",
      clientName : "zz",
      createdDate : "12-12-12",
      quantity : "90",
      categoryId : "other",
      category : "other",
      sectorId : "camp",
      tva : "8%",
      ht : "8$"
    },
  ] */

  // whether: Object;

  chart = []; // This will hold our chart info

  constructor(/* private data: DataService ,  */private _weather: WheatherServiceService) { }

  ngOnInit() {

    this._weather.dailyForecast()
      .subscribe(res => {
      
        // let temp_max = res['list'].map(res => res.main.temp_max);
        // let temp_min = res['list'].map(res => res.main.temp_min);
        // let alldates = res['list'].map(res => res.dt)
        
                //  let weatherDates = []
                //  alldates.forEach((res) => {
                //      let jsdate = new Date(res * 1000)
                //      weatherDates.push(jsdate.toLocaleTimeString('en', { year: 'numeric', month: 'short', day: 'numeric' }))
                //  })
       
      })


          this.chart = new chart('canvas', {
           
            type: 'line',
           
            data: {
              id: '1',
              datasets: [
                { 
                  data: "ZZ",
                  borderColor: "#3cba9f",
                  fill: false
                },
                { 
                  data: "11",
                  borderColor: "#ffcc00",
                  fill: false
                },
              ]
            },

            options: {
              legend: {
                display: false
              },
              scales: {
                xAxes: [{
                  display: true
                }],
                yAxes: [{
                  display: true
                }],
              }
            }
          });

    


    /* this.data.getUsers().subscribe(data => {

      this.users = data
      console.log(this.users);

    }) */


  }

}
