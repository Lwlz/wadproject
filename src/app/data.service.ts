import { Injectable } from '@angular/core';
//access http client ici
import {HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  //modififed
  constructor(private http : HttpClient) { }


  // dailyForecast() {
  //   return this.http.get("https://jsonplaceholder.typicode.com/todos")
  //     .pipe(map(result => result));
  // }

  getUsers(){
    return this.http.get('https://reqres.in/api/users');
  }

  getSales(){
    return this.http.get('https://reqres.in/api/users');
  }

  getStats(){
    return this.http.get('https://reqres.in/api/users');
  }
  //get clients
  getClients(username){
    return this.http.get('localhost:3000/rest/Clients?name=' + username);
  }


}
